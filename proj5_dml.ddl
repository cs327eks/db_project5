-- Generated by Oracle SQL Developer Data Modeler 4.1.3.901
--   at:        2016-11-03 14:16:24 CDT
--   site:      Oracle Database 11g
--   type:      Oracle Database 11g

---dml

DROP TABLE color CASCADE CONSTRAINTS ;

DROP TABLE color_species CASCADE CONSTRAINTS ;

DROP TABLE family CASCADE CONSTRAINTS ;

DROP TABLE genus CASCADE CONSTRAINTS ;

DROP TABLE gss CASCADE CONSTRAINTS ;

DROP TABLE sighting CASCADE CONSTRAINTS ;

DROP TABLE species CASCADE CONSTRAINTS ;

CREATE TABLE color
  ( color_id INTEGER NOT NULL , color_name VARCHAR2 (256)
  ) ;
ALTER TABLE color ADD CONSTRAINT color_PK PRIMARY KEY ( color_id ) ;


CREATE TABLE color_species
  (
    color_species_id INTEGER NOT NULL ,
    species_id       INTEGER NOT NULL ,
    color_id         INTEGER NOT NULL
  ) ;
ALTER TABLE color_species ADD CONSTRAINT color_species_PK PRIMARY KEY ( color_species_id ) ;


CREATE TABLE family
  (
    family_id   INTEGER NOT NULL ,
    family_name VARCHAR2 (256)
  ) ;
ALTER TABLE family ADD CONSTRAINT division_PK PRIMARY KEY ( family_id ) ;


CREATE TABLE genus
  (
    genus_id   INTEGER NOT NULL ,
    genus_name VARCHAR2 (256) ,
    family_id  INTEGER NOT NULL
  ) ;
ALTER TABLE genus ADD CONSTRAINT genus_PK PRIMARY KEY ( genus_id ) ;


CREATE TABLE gss
  (
    gss_id      INTEGER NOT NULL ,
    sighting_id INTEGER NOT NULL ,
    species_id  INTEGER NOT NULL ,
    genus_id    INTEGER NOT NULL
  ) ;
ALTER TABLE gss ADD CONSTRAINT GSS_PK PRIMARY KEY ( gss_id ) ;


CREATE TABLE sighting
  (
    sighting_id INTEGER NOT NULL ,
    WHEN       DATE ,
    location    VARCHAR2 (256)
  ) ;
ALTER TABLE sighting ADD CONSTRAINT sighting_PK PRIMARY KEY ( sighting_id ) ;


CREATE TABLE species
  (
    species_id     INTEGER NOT NULL ,
    species_name   VARCHAR2 (256) ,
    characteristic VARCHAR2 (256) ,
    genus_id       INTEGER NOT NULL ,
    stem           VARCHAR2 (256) ,
    habitat        VARCHAR2 (256) ,
    type           VARCHAR2 (8) NOT NULL
  ) ;
  
  
ALTER TABLE species ADD CONSTRAINT CH_INH_species CHECK ( type IN ('animalia', 'plantae', 'species')) ;
ALTER TABLE species ADD CONSTRAINT species_ExDep CHECK ( (type = 'animalia' AND stem IS NULL) OR (type = 'plantae' AND habitat IS NULL) OR (type = 'species' AND stem IS NULL AND habitat IS NULL)) ;
CREATE UNIQUE INDEX species__IDX ON species ( genus_id ASC ) ;
ALTER TABLE species ADD CONSTRAINT species_PK PRIMARY KEY ( species_id ) ;


ALTER TABLE genus ADD CONSTRAINT Relation_11 FOREIGN KEY ( family_id ) REFERENCES family ( family_id ) ;

ALTER TABLE species ADD CONSTRAINT Relation_12 FOREIGN KEY ( genus_id ) REFERENCES genus ( genus_id ) ;

ALTER TABLE gss ADD CONSTRAINT Relation_10 FOREIGN KEY ( sighting_id ) REFERENCES sighting ( sighting_id ) ;

ALTER TABLE gss ADD CONSTRAINT Relation_13 FOREIGN KEY ( genus_id ) REFERENCES genus ( genus_id ) ;

ALTER TABLE gss ADD CONSTRAINT Relation_9 FOREIGN KEY ( species_id ) REFERENCES species ( species_id ) ;

ALTER TABLE color_species ADD CONSTRAINT Relation_14 FOREIGN KEY ( species_id ) REFERENCES species ( species_id ) ;

ALTER TABLE color_species ADD CONSTRAINT Relation_15 FOREIGN KEY ( color_id ) REFERENCES color ( color_id ) ;

  
CREATE SEQUENCE color_color_id_SEQ START WITH 100 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER color_color_id_TRG BEFORE
  INSERT ON color FOR EACH ROW WHEN (NEW.color_id IS NULL) BEGIN :NEW.color_id := color_color_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE color_species_color_species_id START WITH 100 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER color_species_color_species_id BEFORE
  INSERT ON color_species FOR EACH ROW WHEN (NEW.color_species_id IS NULL) BEGIN :NEW.color_species_id := color_species_color_species_id.NEXTVAL;
END;
/

CREATE SEQUENCE family_family_id_SEQ START WITH 100 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER family_family_id_TRG BEFORE
  INSERT ON family FOR EACH ROW WHEN (NEW.family_id IS NULL) BEGIN :NEW.family_id := family_family_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE genus_genus_id_SEQ START WITH 100 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER genus_genus_id_TRG BEFORE
  INSERT ON genus FOR EACH ROW WHEN (NEW.genus_id IS NULL) BEGIN :NEW.genus_id := genus_genus_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE gss_gss_id_SEQ START WITH 100 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER gss_gss_id_TRG BEFORE
  INSERT ON gss FOR EACH ROW WHEN (NEW.gss_id IS NULL) BEGIN :NEW.gss_id := gss_gss_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE sighting_sighting_id_SEQ START WITH 100 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER sighting_sighting_id_TRG BEFORE
  INSERT ON sighting FOR EACH ROW WHEN (NEW.sighting_id IS NULL) BEGIN :NEW.sighting_id := sighting_sighting_id_SEQ.NEXTVAL;
END;
/

CREATE SEQUENCE species_species_id_SEQ START WITH 100 NOCACHE ORDER ;
CREATE OR REPLACE TRIGGER species_species_id_TRG BEFORE
  INSERT ON species FOR EACH ROW WHEN (NEW.species_id IS NULL) BEGIN :NEW.species_id := species_species_id_SEQ.NEXTVAL;
END;
/
  
--disable FK constraints  
alter table color_species disable constraint Relation_15;
alter table color_species disable constraint Relation_14;
alter table genus disable constraint Relation_11;
alter table gss disable constraint Relation_10;
alter table gss disable constraint Relation_13;
alter table species disable constraint Relation_12;
alter table gss disable constraint Relation_9;

--truncate  
truncate table color;
truncate table color_species;
truncate table family;
truncate table genus;
truncate table gss;
truncate table species;
truncate table sighting;


--insert species
insert into color(color_id, color_name) Values (1, 'gold') ;
insert into color_species(color_species_id, species_id, color_id) Values (1, 1, 1) ;
insert into family(family_id, family_name) Values (1, 'cyprinidae') ;
insert into genus(genus_id, genus_name, family_id) Values (1, 'carassius', 1) ;
insert into species(species_id, species_name, characteristic, genus_id, stem, habitat, type) Values (1, 'Goldfish', 'domesticated', 1, NULL, 'Domestic', 'animalia') ;
insert into sighting(sighting_id, when, location) Values (1, '01-Apr-2008', 'Austin, Texas') ;
insert into gss(gss_id, sighting_id, species_id, genus_id) Values (1, 1, 1, 1) ;

insert into color(color_id, color_name) Values (2, 'green') ;
insert into color_species(color_species_id, species_id, color_id) Values (2, 2, 2) ;
insert into family(family_id, family_name) Values (5, 'cyprinidae') ;
insert into genus(genus_id, genus_name, family_id) Values (2, 'hemitremia', 5) ;
insert into species(species_id, species_name, characteristic, genus_id, stem, habitat, type) Values (2, 'Flame Chub', 'golden stripes' , 2, NULL, 'aquatic', 'animalia') ;
insert into sighting(sighting_id, when, location) Values (2, '13-Oct-2016', 'Corpus Christi, Texas') ;
insert into gss(gss_id, sighting_id, species_id, genus_id) Values (2, 2, 2, 2);


--enable Fk constraints
alter table color_species enable constraint Relation_15;
alter table color_species enable constraint Relation_14;
alter table genus enable constraint Relation_11;
alter table gss enable constraint Relation_10;
alter table gss enable constraint Relation_13;
alter table species enable constraint Relation_12;
alter table gss enable constraint Relation_9;

-- Oracle SQL Developer Data Modeler Summary Report: 
-- 
-- CREATE TABLE                             7
-- CREATE INDEX                             1
-- ALTER TABLE                             16
-- CREATE VIEW                              1
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           7
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          7
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
